package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.RoleDao;
import edu.flst.ipiiweb.service.RoleService;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RoleDto;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public class RoleServiceImpl implements RoleService {

    private Mapper mapperService;

    private RoleDao roleDao;

    private RoleServiceImpl() {
        mapperService = new DozerBeanMapper();
        roleDao = new RoleDao();
    }

    /** Instance unique non préinitialisée */
    private static RoleServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static RoleServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new RoleServiceImpl();
        }
        return INSTANCE;
    }


    @Override
    public RoleDto get(int id) {
        return mapperService.map(roleDao.findById(id), RoleDto.class);
    }

    @Override
    public RoleDto getFirst() {
        return mapperService.map(roleDao.findById(1), RoleDto.class);
    }

    @Override
    public List<RoleDto> getAllRoles() {
        return null;
    }
}
