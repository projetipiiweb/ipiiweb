package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RoleDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public interface RoleService {

    public RoleDto get(int id);

    public RoleDto getFirst();

    public List<RoleDto> getAllRoles();

    //public RoleDto saveRole(RoleDto role);

    //public boolean deleteRole(int id);
}
