package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModeLivraisonDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public interface ModeLivraisonService {

    public List<ModeLivraisonDto> getAllModeLivraison();

    public ModeLivraisonDto saveModeLivraison(ModeLivraisonDto modeLivraison);
}
