package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;

import java.util.List;

/**
 * Created by Dimitri on 10/05/2014.
 */
public interface UserService {

    public UserDto getFirst();

    public List<UserDto> getAllUsers();

    public List<UserDto> getAllValidUsers();

    public UserDto getByUsernameAndPassword(String userName, String password);

    public UserDto saveUser(UserDto user);

    public boolean deleteUser(UserDto user);
}
