package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.ArticleDao;
import edu.flst.ipiiweb.persistence.Article;
import edu.flst.ipiiweb.service.ArticleService;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;
import edu.flst.ipiiweb.transverse.utils.mapper.MapperUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public class ArticleServiceImpl implements ArticleService {

    private Mapper mapperService;

    private ArticleDao articleDao;

    private ArticleServiceImpl() {
        mapperService = new DozerBeanMapper();
        articleDao = new ArticleDao();
    }

    /** Instance unique non préinitialisée */
    private static ArticleServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static ArticleServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new ArticleServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public ArticleDto getFirst() {
        return mapperService.map(articleDao.findById(1), ArticleDto.class);
    }

    @Override
    public List<ArticleDto> getAllArticles() {

        List<Article> articles = articleDao.findAll();

        if (null == articles) {
            return null;
        }

        return MapperUtils.map(articles, mapperService, ArticleDto.class);
    }

    @Override
    public List<ArticleDto> getAllValidArticles() {
        List<Article> articles = articleDao.findAllValidArticles();

        if (null == articles) {
            return null;
        }

        return MapperUtils.map(articles, mapperService, ArticleDto.class);
    }

    @Override
    public ArticleDto saveArticle(ArticleDto article) {
        return mapperService.map(articleDao.save(
                mapperService.map(article, Article.class)), ArticleDto.class);
    }

    @Override
    public boolean deleteArticle(ArticleDto article) {

        article.setDesactive(true);

        if (null != articleDao.save(mapperService.map(article, Article.class))) {
            return true;
        }

        return false;
    }
}
