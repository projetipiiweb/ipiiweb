package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.ModeLivraisonDao;
import edu.flst.ipiiweb.persistence.ModeLivraison;
import edu.flst.ipiiweb.service.ModeLivraisonService;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModeLivraisonDto;
import edu.flst.ipiiweb.transverse.utils.mapper.MapperUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class ModeLivraisonServiceImpl implements ModeLivraisonService {

    private Mapper mapperService;

    private ModeLivraisonDao modeLivraisonDao;

    private ModeLivraisonServiceImpl() {
        mapperService = new DozerBeanMapper();
        modeLivraisonDao = new ModeLivraisonDao();
    }

    /** Instance unique non préinitialisée */
    private static ModeLivraisonServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static ModeLivraisonServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new ModeLivraisonServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public List<ModeLivraisonDto> getAllModeLivraison() {

        List<ModeLivraison> modesLivraison = modeLivraisonDao.findAll();

        if (null == modesLivraison) {
            return null;
        }

        return MapperUtils.map(modesLivraison, mapperService, ModeLivraisonDto.class);
    }


    @Override
    public ModeLivraisonDto saveModeLivraison(ModeLivraisonDto modeLivraison) {
        return mapperService.map(modeLivraisonDao.save(
                mapperService.map(modeLivraison, ModeLivraison.class)), ModeLivraisonDto.class);
    }
}
