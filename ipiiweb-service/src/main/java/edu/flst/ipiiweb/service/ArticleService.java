package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;

import java.util.List;


/**
 * Created by Dgeo on 12/05/2014.
 */
public interface ArticleService {

    public ArticleDto getFirst();

    public List<ArticleDto> getAllArticles();

    public List<ArticleDto> getAllValidArticles();

    public ArticleDto saveArticle(ArticleDto article);

    public boolean deleteArticle(ArticleDto id);

}
