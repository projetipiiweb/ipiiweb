package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.RelaiDao;
import edu.flst.ipiiweb.persistence.Relai;
import edu.flst.ipiiweb.persistence.User;
import edu.flst.ipiiweb.service.RelaiService;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RelaiDto;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RoleDto;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;
import edu.flst.ipiiweb.transverse.utils.mapper.MapperUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class RelaiServiceImpl implements RelaiService {

    private Mapper mapperService;

    private RelaiDao relaiDao;

    private RelaiServiceImpl() {
        mapperService = new DozerBeanMapper();
        relaiDao = new RelaiDao();
    }

    /** Instance unique non préinitialisée */
    private static RelaiServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static RelaiServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new RelaiServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public List<RelaiDto> getAllRelai() {

        List<Relai> relais = relaiDao.findAll();

        if (null == relais) {
            return null;
        }

        return MapperUtils.map(relais, mapperService, RelaiDto.class);
    }


    @Override
    public RelaiDto saveRelai(RelaiDto relai) {
        return mapperService.map(relaiDao.save(
                mapperService.map(relai, Relai.class)), RelaiDto.class);
    }

    @Override
    public boolean deleteRelai(RelaiDto relai) {
        relai.setDesactive(true);

        if (null != relaiDao.save(mapperService.map(relai, Relai.class))) {
            return true;
        }

        return false;
    }

    @Override
    public List<RelaiDto> getAllValidRelai() {
        List<Relai> relais = relaiDao.findAllValidRelai();

        if (relais == null)
            return new ArrayList<RelaiDto>();

        return MapperUtils.map(relais, mapperService, RelaiDto.class);
    }
}
