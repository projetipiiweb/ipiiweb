package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RelaiDto;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RoleDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public interface RelaiService {

    public List<RelaiDto> getAllRelai();

    public RelaiDto saveRelai(RelaiDto relai);

    public boolean deleteRelai(RelaiDto relai);

    public List<RelaiDto> getAllValidRelai();
}
