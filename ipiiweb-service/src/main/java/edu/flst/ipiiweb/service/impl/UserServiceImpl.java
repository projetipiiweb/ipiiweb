package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.UserDao;
import edu.flst.ipiiweb.persistence.User;
import edu.flst.ipiiweb.service.UserService;
import edu.flst.ipiiweb.transverse.code.UserCodeEnum;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;
import edu.flst.ipiiweb.transverse.utils.mapper.MapperUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImpl implements UserService {


    private Mapper mapperService;

    private UserDao userDao;

    private UserServiceImpl() {
        mapperService = new DozerBeanMapper();
        userDao = new UserDao();
    }

    /** Instance unique non préinitialisée */
    private static UserServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static UserServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new UserServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public UserDto getFirst() {
        return mapperService.map(userDao.findById(1), UserDto.class);
    }

    @Override
    public List<UserDto> getAllUsers() {
        List<User> users = userDao.findAll();

        if (users == null)
            return new ArrayList<UserDto>();

        return MapperUtils.map(users, mapperService, UserDto.class);
    }

    @Override
    public List<UserDto> getAllValidUsers() {
        List<User> users = userDao.findAllValidUsers();

        if (users == null)
            return new ArrayList<UserDto>();

        return MapperUtils.map(users, mapperService, UserDto.class);
    }

    @Override
    public UserDto getByUsernameAndPassword(String userName, String password) {
        return mapperService.map(
                userDao.findByLoginAndPassword(userName, password),
                UserDto.class);
    }

    @Override
    public UserDto saveUser(UserDto user) {

        // Admins limités à 20
        if (user.getRole().getId() == UserCodeEnum.ADMINISTRATEUR && userDao.findAdmins().size() > 20) {
            return null;
        }

        User modifiedUser = userDao.save(mapperService.map(user, User.class));

        userDao.flush();

        return mapperService.map(modifiedUser, UserDto.class);
    }

    @Override
    public boolean deleteUser(UserDto user) {

        user.setDesactive(true);

        if (null != userDao.save(mapperService.map(user, User.class))) {
            return true;
        }

        return false;
    }

}
