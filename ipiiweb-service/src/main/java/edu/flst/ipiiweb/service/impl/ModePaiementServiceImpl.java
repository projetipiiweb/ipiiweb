package edu.flst.ipiiweb.service.impl;

import edu.flst.ipiiweb.dao.ModePaiementDao;
import edu.flst.ipiiweb.persistence.ModePaiement;
import edu.flst.ipiiweb.service.ModePaiementService;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModePaiementDto;
import edu.flst.ipiiweb.transverse.utils.mapper.MapperUtils;
import org.dozer.DozerBeanMapper;
import org.dozer.Mapper;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class ModePaiementServiceImpl implements ModePaiementService {

    private Mapper mapperService;

    private ModePaiementDao modePaiementDao;

    private ModePaiementServiceImpl() {
        mapperService = new DozerBeanMapper();
        modePaiementDao = new ModePaiementDao();
    }

    /** Instance unique non préinitialisée */
    private static ModePaiementServiceImpl INSTANCE = null;

    /** Point d'accès pour l'instance unique du singleton */
    public static ModePaiementServiceImpl getInstance()
    {
        if (INSTANCE == null)
        { 	INSTANCE = new ModePaiementServiceImpl();
        }
        return INSTANCE;
    }

    @Override
    public List<ModePaiementDto> getAllModePaiement() {

        List<ModePaiement> modePaiements = modePaiementDao.findAll();

        if (null == modePaiements) {
            return null;
        }

        return MapperUtils.map(modePaiements, mapperService, ModePaiementDto.class);
    }


    @Override
    public ModePaiementDto saveModePaiement(ModePaiementDto modePaiement) {
        return mapperService.map(modePaiementDao.save(
                mapperService.map(modePaiement, ModePaiement.class)), ModePaiementDto.class);
    }
}
