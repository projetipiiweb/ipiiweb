package edu.flst.ipiiweb.service;

import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModePaiementDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public interface ModePaiementService {

    public List<ModePaiementDto> getAllModePaiement();

    public ModePaiementDto saveModePaiement(ModePaiementDto modePaiement);
}
