package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 12/05/2014.
 */
public class ModeLivraisonDto {

    private int id;
    private String libelle;
    private float coutClassique;
    private float coutSuper;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public float getCoutClassique() {
        return coutClassique;
    }

    public void setCoutClassique(float coutClassique) {
        this.coutClassique = coutClassique;
    }

    public float getCoutSuper() {
        return coutSuper;
    }

    public void setCoutSuper(float coutSuper) {
        this.coutSuper = coutSuper;
    }

    @Override
    public String toString() {
        return "ModeLivraisonDto{" +
                "id=" + id +
                ", libelle=" + libelle +
                ", coutClassique=" + coutClassique +
                ", coutSuper=" + coutSuper +
                '}';
    }
}
