package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

import java.util.Collection;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class CommandeDto {

    private int id;
    private String adresse;
    private RelaiDto relai;
    private UserDto user;
    private ModePaiementDto modePaiement;
    private ModeLivraisonDto modeLivraison;
    private Collection<CommandeArticleDto> commandes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public RelaiDto getRelai() {
        return relai;
    }

    public void setRelai(RelaiDto relai) {
        this.relai = relai;
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }

    public ModePaiementDto getModePaiement() {
        return modePaiement;
    }

    public void setModePaiement(ModePaiementDto modePaiement) {
        this.modePaiement = modePaiement;
    }

    public ModeLivraisonDto getModeLivraison() {
        return modeLivraison;
    }

    public void setModeLivraison(ModeLivraisonDto modeLivraison) {
        this.modeLivraison = modeLivraison;
    }

    public Collection<CommandeArticleDto> getCommandes() {
        return commandes;
    }

    public void setCommandes(Collection<CommandeArticleDto> commandes) {
        this.commandes = commandes;
    }

    @Override
    public String toString() {
        return "CommandeDto{" +
                "id=" + id +
                ", adresse='" + adresse + '\'' +
                ", relai=" + relai +
                ", user=" + user +
                ", modePaiement=" + modePaiement +
                ", modeLivraison=" + modeLivraison +
                ", commandes=" + commandes +
                '}';
    }
}
