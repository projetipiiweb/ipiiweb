package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 12/05/2014.
 */
public class RelaiDto {

    private int id;
    private String titre;
    private String adresse;
    private String complement;
    private String codepostal;
    private String ville;
    private boolean desactive;
    //private Collection<CommandeDto> commandes;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public boolean isDesactive() {
        return desactive;
    }

    public void setDesactive(boolean desactive) {
        this.desactive = desactive;
    }

    /*public Collection<CommandeDto> getCommandes() {
        return commandes;
    }

    public void setCommandes(Collection<CommandeDto> commandes) {
        this.commandes = commandes;
    }*/

    @Override
    public String toString() {
        return "RelaiDto{" +
                "id=" + id +
                ", titre='" + titre + '\'' +
                ", adresse='" + adresse + '\'' +
                ", complement='" + complement + '\'' +
                ", codepostal='" + codepostal + '\'' +
                ", ville='" + ville + '\'' +
               // ", commandes=" + commandes +
                '}';
    }
}
