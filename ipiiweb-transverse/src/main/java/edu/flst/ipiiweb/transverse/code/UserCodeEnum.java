package edu.flst.ipiiweb.transverse.code;

/**
 * Created by Dimitri on 12/05/2014.
 */
public class UserCodeEnum {
    public static final int UTILISATEUR = 1;
    public static final int ADMINISTRATEUR = 2;
    public static final int SUPERUTILISATEUR = 3;
}
