package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 12/05/2014.
 */
public class ModePaiementDto {

    private int id;
    private String titre;
    private boolean accessibleClassique;
    private boolean accessibleSuper;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public boolean getAccessibleClassique() {
        return accessibleClassique;
    }

    public void setAccessibleClassique(boolean accessibleClassique) {
        this.accessibleClassique = accessibleClassique;
    }

    public boolean getAccessibleSuper() {
        return accessibleSuper;
    }

    public void setAccessibleSuper(boolean accessibleSuper) {
        this.accessibleSuper = accessibleSuper;
    }

    @Override
    public String toString() {
        return "ModePaiementDto{" +
                "id=" + id +
                ", titre=" + titre +
                ", accessibleClassique=" + accessibleClassique +
                ", accessibleSuper=" + accessibleSuper +
                '}';
    }
}
