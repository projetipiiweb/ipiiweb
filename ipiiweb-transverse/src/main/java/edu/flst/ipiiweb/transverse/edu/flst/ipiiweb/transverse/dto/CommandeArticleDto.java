package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class CommandeArticleDto {

    private int id;
    private float prix;
    private int quantite;
    //private CommandeDto commande;
    private ArticleDto article;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    /*public CommandeDto getCommande() {
        return commande;
    }

    public void setCommande(CommandeDto commande) {
        this.commande = commande;
    }*/

    public ArticleDto getArticle() {
        return article;
    }

    public void setArticle(ArticleDto article) {
        this.article = article;
    }

    @Override
    public String toString() {
        return "CommandeArticleDto{" +
                "id=" + id +
                ", prix=" + prix +
                ", quantite=" + quantite +
                //", commande=" + commande +
                ", article=" + article +
                '}';
    }
}
