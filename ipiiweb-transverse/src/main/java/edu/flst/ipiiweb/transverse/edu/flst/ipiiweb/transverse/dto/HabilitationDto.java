package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class HabilitationDto {

    private int id;
    private String libelle;
    //private Collection<RoleDto> roles;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /*public Collection<RoleDto> getRoles() {
        return roles;
    }

    public void setRoles(Collection<RoleDto> roles) {
        this.roles = roles;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HabilitationDto that = (HabilitationDto) o;

        if (id != that.id) return false;
        if (!libelle.equals(that.libelle)) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + libelle.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "HabilitationDto{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
