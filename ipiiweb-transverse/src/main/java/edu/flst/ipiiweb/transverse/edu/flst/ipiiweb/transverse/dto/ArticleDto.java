package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

import java.util.Collection;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class ArticleDto {

    private int id;
    private String reference;
    private String titre;
    private String descriptionCourte;
    private String descriptionLongue;
    private float prix;
    private int stock;
    private String image;
    private boolean isSuperclientRestricted;
    private Collection<CodePromoDto> codesPromo;
    private String Marque;
    private boolean desactive;


    public String getMarque() {
        return Marque;
    }

    public void setMarque(String marque) {
        Marque = marque;
    }



    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescriptionCourte() {
        return descriptionCourte;
    }

    public void setDescriptionCourte(String descriptionCourte) {
        this.descriptionCourte = descriptionCourte;
    }

    public String getDescriptionLongue() {
        return descriptionLongue;
    }

    public void setDescriptionLongue(String descriptionLongue) {
        this.descriptionLongue = descriptionLongue;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean getIsSuperclientRestricted() {
        return isSuperclientRestricted;
    }

    public void setIsSuperclientRestricted(boolean isSuperclientRestricted) {
        this.isSuperclientRestricted = isSuperclientRestricted;
    }

    public Collection<CodePromoDto> getCodesPromo() {
        return codesPromo;
    }

    public void setCodesPromo(Collection<CodePromoDto> codesPromo) {
        this.codesPromo = codesPromo;
    }

    public boolean isDesactive() {
        return desactive;
    }

    public void setDesactive(boolean desactive) {
        this.desactive = desactive;
    }

    public boolean isSuperclientRestricted() {
        return isSuperclientRestricted;
    }

    public void setSuperclientRestricted(boolean isSuperclientRestricted) {
        this.isSuperclientRestricted = isSuperclientRestricted;
    }
}
