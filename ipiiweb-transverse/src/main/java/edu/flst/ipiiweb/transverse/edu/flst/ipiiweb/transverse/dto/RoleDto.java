package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class RoleDto {

    private int id;
    private String libelle;
    //private Set<HabilitationDto> habilitations;
    //private Set<UserDto> users;

    public RoleDto() {

    }

    public RoleDto(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    /*public Set<HabilitationDto> getHabilitations() {
        return habilitations;
    }

    public void setHabilitations(Set<HabilitationDto> habilitations) {
        this.habilitations = habilitations;
    }

    public Set<UserDto> getUsers() {
        return users;
    }

    public void setUsers(Set<UserDto> users) {
        this.users = users;
    }*/

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RoleDto roleDto = (RoleDto) o;

        if (id != roleDto.id) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return id;
    }

    @Override
    public String toString() {
        return "RoleDto{" +
                "id=" + id +
                ", libelle='" + libelle + '\'' +
                '}';
    }
}
