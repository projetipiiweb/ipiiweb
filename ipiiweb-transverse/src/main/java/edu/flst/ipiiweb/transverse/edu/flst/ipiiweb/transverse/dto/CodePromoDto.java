package edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto;

import java.sql.Date;

/**
 * Created by Dimitri on 12/05/2014.
 */
public class CodePromoDto {

    private int id;
    private String enseigne;
    private String codeId;
    private Date dateDebutValide;
    private Date dateFinValide;
    private String typePromo;
    private float pourcentageReduction;
    //private ArticleDto article;


}
