package edu.flst.ipiiweb.transverse.utils.mapper;

import java.util.ArrayList;
import java.util.List;

import org.dozer.Mapper;

public class MapperUtils {

    public static <T, S> List<T> map(List<S> objects, Mapper mapper,
                                     Class<T> newObjectClass) {
        final List<T> newObjects = new ArrayList<T>();
        for (S s : objects) {
            newObjects.add(mapper.map(s, newObjectClass));
        }
        return newObjects;
    }
}
