package edu.flst.ipiiweb.filter;

import edu.flst.ipiiweb.bean.Login;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Filter checks if Login has loginIn property set to true. If it is not set
 * then request is being redirected to the login.xhml page.
 *
 * @author dleguis
 *
 */
public class LoginFilter implements Filter {

    /**
     * Checks if user is logged in. If not it redirects to the login.xhtml page.
     */
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        // Get the loginBean from session attribute
        Login loginBean = (Login) ((HttpServletRequest) request).getSession()
                .getAttribute("login");

        // For the first application request there is no loginBean in the
        // session so user needs to log in
        // For other requests loginBean is present but we need to check if user
        // has logged in successfully
        if (loginBean == null || !loginBean.isLoggedIn()) {
            String contextPath = ((HttpServletRequest) request)
                    .getContextPath();
            ((HttpServletResponse) response).sendRedirect(contextPath
                    + "/login.jsf");
        } else {
            chain.doFilter(request, response);
        }

    }

    public void init(FilterConfig config) throws ServletException {
        // Nothing to do here!
    }

    public void destroy() {
        // Nothing to do here!
    }

}
