package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.RoleService;
import edu.flst.ipiiweb.service.UserService;
import edu.flst.ipiiweb.service.impl.RoleServiceImpl;
import edu.flst.ipiiweb.service.impl.UserServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by Dimitri on 13/05/2014.
 */
public class ModifierUtilisateurBean {

    private UserDto currentUser;


    private UserService userService;
    private RoleService roleService;

    public ModifierUtilisateurBean() {
        userService = UserServiceImpl.getInstance();
        roleService = RoleServiceImpl.getInstance();
    }

    public UserDto getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserDto currentUser) {
        this.currentUser = currentUser;
    }

    public UserService getUserService() {
        return userService;
    }

    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    public RoleService getRoleService() {
        return roleService;
    }

    public void setRoleService(RoleService roleService) {
        this.roleService = roleService;
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String goToDetailPage() {


        return "detail-page";
    }

    public String doSaveUser() {
        if (null != userService.saveUser(currentUser)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" , null));

        return "save-fail";
    }

    public String doDeleteUser() {
        if (userService.deleteUser(currentUser)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La suppression s'est déroulée avec succès" , null));
            return "delete-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La suppression a échoué" , null));

        return "delete-fail";
    }


}
