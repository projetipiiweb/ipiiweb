package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.ArticleService;
import edu.flst.ipiiweb.service.impl.ArticleServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by Dgeo on 13/05/2014.
 */
public class ModifierArticleBean {

    private ArticleDto currentArticle;


    private ArticleService articleService;

    public ModifierArticleBean() {
        articleService = ArticleServiceImpl.getInstance();
    }

    public ArticleDto getCurrentArticle() {
        return currentArticle;
    }

    public void setCurrentArticle(ArticleDto currentArticle) {
        this.currentArticle = currentArticle;
    }

    public ArticleService getArticleService() {
        return articleService;
    }

    public void setArticleService(ArticleService articleService) {
        this.articleService = articleService;
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String goToDetailPage() {
        return "detail-page";
    }

    public String doSaveArticle() {
        if (null != articleService.saveArticle(currentArticle)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" , null));

        return "save-fail";
    }

    public String doDeleteArticle() {

        if (articleService.deleteArticle(currentArticle)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La suppression s'est déroulée avec succès" , null));
            return "delete-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La suppression a échoué" , null));

        return "delete-fail";
    }

}
