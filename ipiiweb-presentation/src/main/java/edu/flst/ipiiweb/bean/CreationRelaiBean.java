package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.RelaiService;
import edu.flst.ipiiweb.service.impl.RelaiServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RelaiDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by Dgeo on 13/05/2014.
 */
public final class CreationRelaiBean {

    private int id;
    private String titre;
    private String adresse;
    private String complement;
    private String codepostal;
    private String ville;
    private boolean desactive;


    private RelaiService relaiService;

    public CreationRelaiBean() {
        relaiService = RelaiServiceImpl.getInstance();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public boolean isDesactive() {
        return desactive;
    }

    public void setDesactive(boolean desactive) {
        this.desactive = desactive;
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }


    public String addRelai(){

        RelaiDto currentRelai = new RelaiDto();

        currentRelai.setId(id);
        currentRelai.setAdresse(adresse);
        currentRelai.setCodepostal(codepostal);
        currentRelai.setComplement(complement);
        currentRelai.setTitre(titre);
        currentRelai.setVille(ville);


        if (null != relaiService.saveRelai(currentRelai)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "L'inscription s'est déroulée avec succès" , null));
            return "success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "L'inscription a échoué" , null));
        return "fail";

    }


}
