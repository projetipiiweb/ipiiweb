package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.ArticleService;
import edu.flst.ipiiweb.service.impl.ArticleServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dgeo on 13/05/2014.
 */
public final class CreationArticleBean {

    private String reference;
    private String titre;
    private String descriptionCourte;
    private String descriptionLongue;
    private float prix;
    private int stock;
    private String image;
    private boolean isSuperclientRestricted;
    @Deprecated
    private String marque;

    private List<String> marques;

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getDescriptionCourte() {
        return descriptionCourte;
    }

    public void setDescriptionCourte(String descriptionCourte) {
        this.descriptionCourte = descriptionCourte;
    }

    public String getDescriptionLongue() {
        return descriptionLongue;
    }

    public void setDescriptionLongue(String descriptionLongue) {
        this.descriptionLongue = descriptionLongue;
    }

    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public boolean isSuperclientRestricted() {
        return isSuperclientRestricted;
    }

    public void setSuperclientRestricted(boolean isSuperclientRestricted) {
        this.isSuperclientRestricted = isSuperclientRestricted;
    }

    public List<String> getMarques() {

        if (marques == null)
            marques = new ArrayList<String>();

        return marques;
    }

    public void setMarques(List<String> marques) {
        this.marques = marques;
    }

    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    private ArticleService articleService;

    public CreationArticleBean() {
        articleService = ArticleServiceImpl.getInstance();
    }




    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }


    public String addArticle(){
        ArticleDto currentArticle = new ArticleDto();
        currentArticle.setReference(this.reference);
        currentArticle.setDescriptionCourte(this.descriptionCourte);
        currentArticle.setTitre(this.titre);
        currentArticle.setStock(this.stock);
        currentArticle.setDescriptionLongue(this.descriptionLongue);
        currentArticle.setPrix(this.prix);
        currentArticle.setImage(this.image);
        currentArticle.setIsSuperclientRestricted(this.isSuperclientRestricted);

        currentArticle.setMarque(this.marque);


        if (null != articleService.saveArticle(currentArticle)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "L'inscription s'est déroulée avec succès" , null));
            return "success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "L'inscription a échoué" , null));
        return "fail";

    }


}
