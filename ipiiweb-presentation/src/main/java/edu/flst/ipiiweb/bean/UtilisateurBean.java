package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.UserService;
import edu.flst.ipiiweb.service.impl.UserServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;

import java.util.List;

public final class UtilisateurBean {

    private List<UserDto> users;

    private UserService userService;

    private UserDto currentUser;

    public UtilisateurBean() {
        userService = UserServiceImpl.getInstance();
        getUsers();
    }

    public List<UserDto> getUsers() {

        //if (users == null) {
            users = userService.getAllValidUsers();
        //}

        // actions intermediaires...?
        return users;
    }

    public String doSaveUser() {
        if (null != userService.saveUser(currentUser)) {
            return "save-success";
        }

        return "save-fail";
    }

    public String doDeleteUser() {
        if (userService.deleteUser(currentUser)) {
            return "delete-success";
        }
        else {
            return "delete-fail";
        }
    }

    public UserDto getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(UserDto currentUser) {
        this.currentUser = currentUser;
    }
}