package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.RelaiService;
import edu.flst.ipiiweb.service.impl.RelaiServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RelaiDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by Dimitri on 14/05/2014.
 */
public class ModifierRelaiBean {

    private RelaiDto currentRelai;

    private RelaiService relaiService;

    public ModifierRelaiBean() {
        relaiService = RelaiServiceImpl.getInstance();
    }

    public RelaiDto getCurrentRelai() {
        return currentRelai;
    }

    public void setCurrentRelai(RelaiDto currentRelai) {
        this.currentRelai = currentRelai;
    }

    public RelaiService getRelaiService() {
        return relaiService;
    }

    public void setRelaiService(RelaiService relaiService) {
        this.relaiService = relaiService;
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    public String goToDetailPage() {
        return "detail-page";
    }

    public String doSaveRelai() {
        if (null != relaiService.saveRelai(currentRelai)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" , null));

        return "save-fail";
    }

    public String doDeleteRelai() {

        if (relaiService.deleteRelai(currentRelai)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La suppression s'est déroulée avec succès" , null));
            return "delete-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La suppression a échoué" , null));

        return "delete-fail";
    }
}
