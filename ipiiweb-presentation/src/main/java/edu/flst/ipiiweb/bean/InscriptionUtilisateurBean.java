package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.RoleService;
import edu.flst.ipiiweb.service.UserService;
import edu.flst.ipiiweb.service.impl.RoleServiceImpl;
import edu.flst.ipiiweb.service.impl.UserServiceImpl;
import edu.flst.ipiiweb.transverse.code.UserCodeEnum;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RoleDto;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class InscriptionUtilisateurBean {

    private String username;
    private String password;
    private String firstname;
    private String lastname;
    private String[] selectedGroups;

    private UserService userService;
    private RoleService roleService;

    public InscriptionUtilisateurBean() {
        userService = UserServiceImpl.getInstance();
        roleService = RoleServiceImpl.getInstance();
    }

    public String getUsername() {

        return username;

    }


    public void setUsername(String username) {
        this.username = username;
    }


    public String getPassword() {
        return password;
    }


    public void setPassword(String password) {
        this.password = password;
    }


    public String getFirstname() {
        return firstname;
    }


    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }


    public String getLastname() {
        return lastname;
    }


    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String[] getSelectedGroups() {
        return selectedGroups;
    }

    public void setSelectedGroups(String[] selectedGroups) {
        this.selectedGroups = selectedGroups;
    }


    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }


    public String addUser(){

        UserDto currentUser = new UserDto();

        currentUser.setUsername(this.username);
        currentUser.setPassword(this.password);
        currentUser.setFirstname(this.firstname);
        currentUser.setLastname(this.lastname);

        RoleDto role = null;

        for (String selectedRole : selectedGroups) {
            if (selectedRole.equals("Administrateur")) {
                role = roleService.get(UserCodeEnum.ADMINISTRATEUR);
            }
            else if (selectedRole.equals("Client")) {
                role = roleService.get(UserCodeEnum.UTILISATEUR);
            }
            else if (selectedRole.equals("Superclient")) {
                role = roleService.get(UserCodeEnum.SUPERUTILISATEUR);
            }
        }

        currentUser.setRole(role);

        if (null != userService.saveUser(currentUser)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "L'inscription s'est déroulée avec succès" , null));
            currentUser = new UserDto();
            return "success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "L'inscription a échoué" , null));
        return "fail";

    }


}
