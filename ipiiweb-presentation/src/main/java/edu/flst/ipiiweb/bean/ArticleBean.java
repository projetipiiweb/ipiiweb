package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.ArticleService;
import edu.flst.ipiiweb.service.impl.ArticleServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class ArticleBean {

    private List<ArticleDto> articles;

    private ArticleService articleService;

    private ArticleDto currentArticle;

    public ArticleBean() {
        articleService = ArticleServiceImpl.getInstance();
        getArticles();
    }

    public List<ArticleDto> getArticles() {

        //if (articles == null) {
            articles = articleService.getAllValidArticles();
        //}

        // actions intermediaires...?
        return articles;
    }

    public String doSaveArticle() {
        if (null != articleService.saveArticle(currentArticle)) {
            return "save-success";
        }

        return "save-fail";
    }

    public String doDeleteArticle() {
        if (articleService.deleteArticle(currentArticle)) {
            return "delete-success";
        }
        else {
            return "delete-fail";
        }
    }

    public ArticleDto getCurrentArticle() {
        return currentArticle;
    }

    public void setCurrentArticle(ArticleDto currentArticle) {
        this.currentArticle = currentArticle;
    }
}
