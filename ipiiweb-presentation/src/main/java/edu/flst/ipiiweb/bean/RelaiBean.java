package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.persistence.Relai;
import edu.flst.ipiiweb.service.ArticleService;
import edu.flst.ipiiweb.service.RelaiService;
import edu.flst.ipiiweb.service.impl.ArticleServiceImpl;
import edu.flst.ipiiweb.service.impl.RelaiServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ArticleDto;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.RelaiDto;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class RelaiBean {

    private List<RelaiDto> relais;

    private RelaiService relaiService;

    private RelaiDto CurrentRelai;

    public RelaiBean() {
        relaiService = RelaiServiceImpl.getInstance();
        getRelai();
    }

    public List<RelaiDto> getRelai() {

        //if (relais == null) {
            relais = relaiService.getAllValidRelai();
        //}

        // actions intermediaires...?
        return relais;
    }

    public String doSaveRelai() {
        if (null != relaiService.saveRelai(CurrentRelai)) {
            return "save-success";
        }

        return "save-fail";
    }

    public String doDeleteRelai() {
        if (relaiService.deleteRelai(CurrentRelai)) {
            return "delete-success";
        }
        else {
            return "delete-fail";
        }
    }

    public RelaiDto getCurrentRelai() {
        return CurrentRelai;
    }

    public void setCurrentRelai(RelaiDto CurrentRelai) {
        this.CurrentRelai = CurrentRelai;
    }

}
