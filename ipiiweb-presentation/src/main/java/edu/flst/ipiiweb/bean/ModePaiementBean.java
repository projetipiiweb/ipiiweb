package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.ModePaiementService;
import edu.flst.ipiiweb.service.impl.ModePaiementServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModePaiementDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public final class ModePaiementBean {

    private List<ModePaiementDto> modesPaiement;

    private ModePaiementService modePaiementService;


    public ModePaiementBean() {
        modePaiementService = ModePaiementServiceImpl.getInstance();
        getModesPaiement();
    }

    public List<ModePaiementDto> getModesPaiement() {

        if (modesPaiement == null) {
            modesPaiement = modePaiementService.getAllModePaiement();
        }
        // actions intermediaires...?
        return modesPaiement;
    }

    public String doSavePaiement() {

        boolean saveError = false;

        for (ModePaiementDto modePaiement : modesPaiement) {
            if (null == modePaiementService.saveModePaiement(modePaiement)) {
                saveError = true;
            }
        }

        if (!saveError) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" ,null));

        return "save-fail";
     }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }
}
