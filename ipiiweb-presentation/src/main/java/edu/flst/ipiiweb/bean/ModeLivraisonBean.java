package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.ModeLivraisonService;
import edu.flst.ipiiweb.service.impl.ModeLivraisonServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.ModeLivraisonDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public class ModeLivraisonBean {

    private List<ModeLivraisonDto> modesLivraison;

    private ModeLivraisonService modeLivraisonService;

    private ModeLivraisonDto currentModeLivraison;

    public ModeLivraisonBean() {
        modeLivraisonService = ModeLivraisonServiceImpl.getInstance();
        getModesLivraison();
    }

    public List<ModeLivraisonDto> getModesLivraison() {

        if (modesLivraison == null) {
            modesLivraison = modeLivraisonService.getAllModeLivraison();
        }

        // actions intermediaires...?
        return modesLivraison;
    }

    public ModeLivraisonDto getCurrentModeLivraison() {
        return currentModeLivraison;
    }

    public void setCurrentModeLivraison(ModeLivraisonDto currentModeLivraison) {
        this.currentModeLivraison = currentModeLivraison;
    }

    public String saveModeLivraison() {

        if (null != modeLivraisonService.saveModeLivraison(currentModeLivraison)) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }


        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" , null));

        return "save-fail";
    }

    public String saveModesLivraison() {
        boolean saveError = false;

        for (ModeLivraisonDto modeLivraison : modesLivraison) {
            if (null == modeLivraisonService.saveModeLivraison(modeLivraison)) {
                saveError = true;
            }
        }

        if (!saveError) {
            addMessage(new FacesMessage(FacesMessage.SEVERITY_INFO, "La modification s'est déroulée avec succès" , null));
            //currentUser = new UserDto();
            return "save-success";
        }

        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "La modification a échoué" , null));

        return "save-fail";
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

}
