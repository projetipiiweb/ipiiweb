package edu.flst.ipiiweb.bean;

import edu.flst.ipiiweb.service.UserService;
import edu.flst.ipiiweb.service.impl.UserServiceImpl;
import edu.flst.ipiiweb.transverse.edu.flst.ipiiweb.transverse.dto.UserDto;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import java.io.Serializable;

public final class Login implements Serializable {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    private UserService userService;


    private String username;
    private String password;

    private boolean loggedIn;

    private NavigationBean navigationBean;

    public Login() {
        userService = UserServiceImpl.getInstance();
    }

    private void addMessage(FacesMessage message){
        FacesContext.getCurrentInstance().addMessage(null, message);
    }

    /**
     * Login operation.
     *
     * @return résultat de l'opération
     */
    public String doLogin() {

        userService = UserServiceImpl.getInstance();

        UserDto user = userService.getByUsernameAndPassword(username, password);

        // Successful login
        if (user != null && user.getId() != 0) {
            loggedIn = true;
            navigationBean = new NavigationBean();
            return "success";// navigationBean.redirectToWelcome();
        }

        // Set login ERROR
        addMessage(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Login Error!" ,null));

        // To to login page
        return "/login.xhtml"; //navigationBean.toLogin();

    }

    /**
     * Logout operation.
     *
     * @return résultat de l'opération
     */
    public String doLogout() {
        // Set the paremeter indicating that user is logged in to false
        loggedIn = false;

        // Set logout message
        FacesMessage msg = new FacesMessage("Logout success!", "INFO MSG");
        msg.setSeverity(FacesMessage.SEVERITY_INFO);
        FacesContext.getCurrentInstance().addMessage(null, msg);

        return navigationBean.toLogin();
    }

    // ------------------------------
    // Getters & Setters

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public void setNavigationBean(NavigationBean navigationBean) {
        this.navigationBean = navigationBean;
    }


}
