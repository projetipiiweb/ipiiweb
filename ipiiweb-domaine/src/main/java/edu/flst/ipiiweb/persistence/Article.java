package edu.flst.ipiiweb.persistence;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
public class Article {
    private int id;
    private String reference;
    private String titre;
    private String descriptionCourte;
    private String descriptionLongue;
    private float prix;
    private int stock;
    private String image;
    private boolean isSuperclientRestricted;
    private boolean desactive;
    private Collection<CodePromo> codesPromo;
    private String marque;






    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "reference")
    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "description_courte")
    public String getDescriptionCourte() {
        return descriptionCourte;
    }

    public void setDescriptionCourte(String descriptionCourte) {
        this.descriptionCourte = descriptionCourte;
    }

    @Basic
    @Column(name = "description_longue")
    public String getDescriptionLongue() {
        return descriptionLongue;
    }

    public void setDescriptionLongue(String descriptionLongue) {
        this.descriptionLongue = descriptionLongue;
    }

    @Basic
    @Column(name = "prix")
    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "stock")
    public int getStock() {
        return stock;
    }

    public void setStock(int stock) {
        this.stock = stock;
    }

    @Basic
    @Column(name = "image")
    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Basic
    @Column(name = "is_superclient_restricted")
    public boolean getIsSuperclientRestricted() {
        return isSuperclientRestricted;
    }

    public void setIsSuperclientRestricted(boolean isSuperclientRestricted) {
        this.isSuperclientRestricted = isSuperclientRestricted;
    }

    @Basic
    @Column(name = "desactive")
    public boolean isDesactive() {
        return desactive;
    }

    public void setDesactive(boolean desactive) {
        this.desactive = desactive;
    }


    @Basic
    @Column(name = "marque")
    public String getMarque() {
        return marque;
    }

    public void setMarque(String marque) {
        this.marque = marque;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Article article = (Article) o;

        if (id != article.id) return false;

        return true;
    }



    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "article", fetch = FetchType.LAZY)
    public Collection<CodePromo> getCodesPromo() {
        return codesPromo;
    }

    public void setCodesPromo(Collection<CodePromo> codesPromo) {
        this.codesPromo = codesPromo;
    }
}
