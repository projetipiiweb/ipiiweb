package edu.flst.ipiiweb.persistence;

import javax.persistence.*;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
@Table(name = "commande_article", schema = "", catalog = "ipiiweb")
public class CommandeArticle {
    private int id;
    private float prix;
    private int quantite;
    private Commande commande;
    private Article article;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "prix")
    public float getPrix() {
        return prix;
    }

    public void setPrix(float prix) {
        this.prix = prix;
    }

    @Basic
    @Column(name = "quantite")
    public int getQuantite() {
        return quantite;
    }

    public void setQuantite(int quantite) {
        this.quantite = quantite;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CommandeArticle that = (CommandeArticle) o;

        if (id != that.id) return false;
        if (Float.compare(that.prix, prix) != 0) return false;
        if (quantite != that.quantite) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (prix != +0.0f ? Float.floatToIntBits(prix) : 0);
        result = 31 * result + quantite;
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "commande_id", referencedColumnName = "id", nullable = false)
    public Commande getCommande() {
        return commande;
    }

    public void setCommande(Commande commande) {
        this.commande = commande;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id", referencedColumnName = "id", nullable = false)
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
