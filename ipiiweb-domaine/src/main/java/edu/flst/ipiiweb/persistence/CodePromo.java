package edu.flst.ipiiweb.persistence;

import javax.persistence.*;
import java.sql.Date;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
@Table(name = "code_promo", schema = "", catalog = "ipiiweb")
public class CodePromo {
    private int id;
    private String enseigne;
    private String codeId;
    private Date dateDebutValide;
    private Date dateFinValide;
    private String typePromo;
    private float pourcentageReduction;
    private Article article;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "enseigne")
    public String getEnseigne() {
        return enseigne;
    }

    public void setEnseigne(String enseigne) {
        this.enseigne = enseigne;
    }

    @Basic
    @Column(name = "code_id")
    public String getCodeId() {
        return codeId;
    }

    public void setCodeId(String codeId) {
        this.codeId = codeId;
    }

    @Basic
    @Column(name = "date_debut_valide")
    public Date getDateDebutValide() {
        return dateDebutValide;
    }

    public void setDateDebutValide(Date dateDebutValide) {
        this.dateDebutValide = dateDebutValide;
    }

    @Basic
    @Column(name = "date_fin_valide")
    public Date getDateFinValide() {
        return dateFinValide;
    }

    public void setDateFinValide(Date dateFinValide) {
        this.dateFinValide = dateFinValide;
    }

    @Basic
    @Column(name = "type_promo")
    public String getTypePromo() {
        return typePromo;
    }

    public void setTypePromo(String typePromo) {
        this.typePromo = typePromo;
    }

    @Basic
    @Column(name = "pourcentage_reduction")
    public float getPourcentageReduction() {
        return pourcentageReduction;
    }

    public void setPourcentageReduction(float pourcentageReduction) {
        this.pourcentageReduction = pourcentageReduction;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CodePromo codePromo = (CodePromo) o;

        if (id != codePromo.id) return false;
        if (Float.compare(codePromo.pourcentageReduction, pourcentageReduction) != 0) return false;
        if (codeId != null ? !codeId.equals(codePromo.codeId) : codePromo.codeId != null) return false;
        if (dateDebutValide != null ? !dateDebutValide.equals(codePromo.dateDebutValide) : codePromo.dateDebutValide != null)
            return false;
        if (dateFinValide != null ? !dateFinValide.equals(codePromo.dateFinValide) : codePromo.dateFinValide != null)
            return false;
        if (enseigne != null ? !enseigne.equals(codePromo.enseigne) : codePromo.enseigne != null) return false;
        if (typePromo != null ? !typePromo.equals(codePromo.typePromo) : codePromo.typePromo != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (enseigne != null ? enseigne.hashCode() : 0);
        result = 31 * result + (codeId != null ? codeId.hashCode() : 0);
        result = 31 * result + (dateDebutValide != null ? dateDebutValide.hashCode() : 0);
        result = 31 * result + (dateFinValide != null ? dateFinValide.hashCode() : 0);
        result = 31 * result + (typePromo != null ? typePromo.hashCode() : 0);
        result = 31 * result + (pourcentageReduction != +0.0f ? Float.floatToIntBits(pourcentageReduction) : 0);
        return result;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "article_id", referencedColumnName = "id", nullable = false)
    public Article getArticle() {
        return article;
    }

    public void setArticle(Article article) {
        this.article = article;
    }
}
