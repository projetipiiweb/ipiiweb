package edu.flst.ipiiweb.persistence;

import javax.persistence.*;
import java.util.Collection;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
public class Relai {
    private int id;
    private String titre;
    private String adresse;
    private String complement;
    private String codepostal;
    private String ville;
    private boolean desactive;
    private Collection<Commande> commandes;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "adresse")
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }

    @Basic
    @Column(name = "complement")
    public String getComplement() {
        return complement;
    }

    public void setComplement(String complement) {
        this.complement = complement;
    }

    @Basic
    @Column(name = "codepostal")
    public String getCodepostal() {
        return codepostal;
    }

    public void setCodepostal(String codepostal) {
        this.codepostal = codepostal;
    }

    @Basic
    @Column(name = "ville")
    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    @Basic
    @Column(name = "desactive")
    public boolean isDesactive() {
        return desactive;
    }

    public void setDesactive(boolean desactive) {
        this.desactive = desactive;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Relai relai = (Relai) o;

        if (id != relai.id) return false;
        if (adresse != null ? !adresse.equals(relai.adresse) : relai.adresse != null) return false;
        if (codepostal != null ? !codepostal.equals(relai.codepostal) : relai.codepostal != null) return false;
        if (complement != null ? !complement.equals(relai.complement) : relai.complement != null) return false;
        if (titre != null ? !titre.equals(relai.titre) : relai.titre != null) return false;
        if (ville != null ? !ville.equals(relai.ville) : relai.ville != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (titre != null ? titre.hashCode() : 0);
        result = 31 * result + (adresse != null ? adresse.hashCode() : 0);
        result = 31 * result + (complement != null ? complement.hashCode() : 0);
        result = 31 * result + (codepostal != null ? codepostal.hashCode() : 0);
        result = 31 * result + (ville != null ? ville.hashCode() : 0);
        return result;
    }

    @OneToMany(mappedBy = "relai", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Collection<Commande> getCommandes() {
        return commandes;
    }

    public void setCommandes(Collection<Commande> commandes) {
        this.commandes = commandes;
    }
}
