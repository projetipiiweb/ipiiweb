package edu.flst.ipiiweb.persistence;

import javax.persistence.*;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
@Table(name = "mode_paiement", schema = "", catalog = "ipiiweb")
public class ModePaiement {
    private int id;
    private String titre;
    private boolean accessibleClassique;
    private boolean accessibleSuper;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "titre")
    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Basic
    @Column(name = "accessible_classique")
    public boolean getAccessibleClassique() {
        return accessibleClassique;
    }

    public void setAccessibleClassique(boolean accessibleClassique) {
        this.accessibleClassique = accessibleClassique;
    }

    @Basic
    @Column(name = "accessible_super")
    public boolean getAccessibleSuper() {
        return accessibleSuper;
    }

    public void setAccessibleSuper(boolean accessibleSuper) {
        this.accessibleSuper = accessibleSuper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModePaiement that = (ModePaiement) o;

        if (id != that.id) return false;
        if (titre != that.titre) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        return result;
    }
}
