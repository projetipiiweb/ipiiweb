package edu.flst.ipiiweb.persistence;

import javax.persistence.*;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
@Table(name = "mode_livraison", schema = "", catalog = "ipiiweb")
public class ModeLivraison {
    private int id;
    private String libelle;
    private float coutClassique;
    private float coutSuper;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Basic
    @Column(name = "cout_classique")
    public float getCoutClassique() {
        return coutClassique;
    }

    public void setCoutClassique(float coutClassique) {
        this.coutClassique = coutClassique;
    }

    @Basic
    @Column(name = "cout_super")
    public float getCoutSuper() {
        return coutSuper;
    }

    public void setCoutSuper(float coutSuper) {
        this.coutSuper = coutSuper;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ModeLivraison that = (ModeLivraison) o;

        if (Float.compare(that.coutClassique, coutClassique) != 0) return false;
        if (Float.compare(that.coutSuper, coutSuper) != 0) return false;
        if (id != that.id) return false;
        if (libelle != that.libelle) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (coutClassique != +0.0f ? Float.floatToIntBits(coutClassique) : 0);
        result = 31 * result + (coutSuper != +0.0f ? Float.floatToIntBits(coutSuper) : 0);
        return result;
    }
}
