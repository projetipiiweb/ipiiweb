package edu.flst.ipiiweb.persistence;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by Dimitri on 12/05/2014.
 */
@Entity
public class Role {
    private int id;
    private String libelle;
    private Set<Habilitation> habilitations;
    private Set<User> users;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "libelle")
    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Role role = (Role) o;

        if (id != role.id) return false;
        if (libelle != null ? !libelle.equals(role.libelle) : role.libelle != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id;
        result = 31 * result + (libelle != null ? libelle.hashCode() : 0);
        return result;
    }

    @ManyToMany(mappedBy = "roles", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Set<Habilitation> getHabilitations() {
        return habilitations;
    }

    public void setHabilitations(Set<Habilitation> habilitations) {
        this.habilitations = habilitations;
    }

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    public Set<User> getUsers() {
        return users;
    }

    public void setUsers(Set<User> users) {
        this.users = users;
    }
}
