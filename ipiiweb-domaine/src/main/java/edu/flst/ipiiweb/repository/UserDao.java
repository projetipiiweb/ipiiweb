package edu.flst.ipiiweb.repository;

import edu.flst.ipiiweb.persistence.User;

import javax.persistence.*;
import java.util.List;


public class UserDao {

    @PersistenceUnit(unitName = "ipiiPU")
    private EntityManagerFactory emf;

    @PersistenceContext(unitName = "ipiiPU")
    private EntityManager em;

    public UserDao() {
        emf = Persistence.createEntityManagerFactory("ipiiPU");
        em = emf.createEntityManager();
    }

    public User findFirst() {

        User user = em.find(User.class, 1);

        // em.close();
        // emf.close();

        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAll() {
        return em.createQuery("select u from User u").getResultList();
    }

    public User findByLoginAndPassword(String login, String password) {
        Query query = em
                .createQuery("select u from User u where u.username = :login and u.password = :password");
        query.setParameter("login", login);
        query.setParameter("password", password);

        // em.close();
        // emf.close();

        return (User) query.getSingleResult();
    }

}
