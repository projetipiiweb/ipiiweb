package edu.flst.ipiiweb.dao;

import edu.flst.ipiiweb.persistence.User;
import edu.flst.ipiiweb.transverse.code.UserCodeEnum;
import org.hibernate.*;

import java.util.List;

/**
 * Created by Dimitri on 11/05/2014.
 */
public class UserDao extends AbstractDao<User, Integer> {

    public User findByLoginAndPassword(String username, String password) {

        Session session = getSession();

        if (username == null || password == null)
            return new User();

        User user = new User();

        try {
            final Query query = session.createQuery("select u from User u where u.username = :username and u.password = :password");
            query.setParameter("username", username);
            query.setParameter("password", password);

            session.setCacheMode(CacheMode.IGNORE);

            user = (User) query.uniqueResult();

            Hibernate.initialize(user.getCommandes());
            Hibernate.initialize(user.getRole());
        }
        catch (Exception e ) {
            //return null
        }
        finally {
            session.close();
        }

        if (user == null) {
            user = new User();
        }


        return user;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAdmins() {

        Session session = getSession();

        // id 2 --> Admin
        List<User> users = session.createQuery("select u from User u where u.role.id = :codeAdmin")
                .setParameter("codeAdmin", UserCodeEnum.ADMINISTRATEUR)
                .setFlushMode(FlushMode.ALWAYS)
                .setCacheMode(CacheMode.REFRESH)
                .list();

        session.setCacheMode(CacheMode.IGNORE);

        session.close();

        return users;
    }

    @SuppressWarnings("unchecked")
    public List<User> findAllValidUsers() {

        Session session = getSession();

        List<User> users = null;

        try {

            users = session.createQuery("select u from User u where u.desactive = :isDesactive")
                    .setParameter("isDesactive", false)
                    .setFlushMode(FlushMode.ALWAYS)
                    .setCacheMode(CacheMode.REFRESH)
                    .list();

            session.setCacheMode(CacheMode.IGNORE);

            for (User user : users) {
                Hibernate.initialize(user.getCommandes());
                Hibernate.initialize(user.getRole());
            }

        }
        catch (Exception e ) {
            //return null
        }
        finally {
            session.close();
        }

        return users;
    }

    public User deleteUser(int id) {

        Session session = getSession();

        Transaction tx = null;

        User u;

        try {
            tx = getSession().beginTransaction();

            u = (User) session.createQuery("update User u set u.desactive = :isDesactive where u.id = :userId")
                    .setParameter("isDesactive", true)
                    .setParameter("userId", id)
                    .setFlushMode(FlushMode.ALWAYS)
                    .setCacheMode(CacheMode.REFRESH)
                    .uniqueResult();

            tx.commit();
        }
        catch (HibernateException e) {
            if (tx!=null) {
                tx.rollback();
            }
            return null;
        }
        finally {
            session.close();
        }

        return u;
    }
}
