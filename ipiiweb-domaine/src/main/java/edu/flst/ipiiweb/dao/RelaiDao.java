package edu.flst.ipiiweb.dao;

import edu.flst.ipiiweb.persistence.Relai;
import edu.flst.ipiiweb.persistence.User;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import java.util.List;

/**
 * Created by Dimitri on 13/05/2014.
 */
public class RelaiDao extends AbstractDao<Relai, Integer> {

    public List<Relai> findAllValidRelai() {

        Session session = getSession();

        List<Relai> relais = null;

        try {

            relais = session.createQuery("select r from Relai r where r.desactive = :isDesactive")
                    .setParameter("isDesactive", false)
                    .setFlushMode(FlushMode.ALWAYS)
                    .setCacheMode(CacheMode.REFRESH)
                    .list();

            session.setCacheMode(CacheMode.IGNORE);


        }
        catch (Exception e ) {
            //return null
        }
        finally {
            session.close();
        }

        return relais;
    }

}
