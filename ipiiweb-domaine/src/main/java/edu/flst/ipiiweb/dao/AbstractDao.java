package edu.flst.ipiiweb.dao;

import org.hibernate.*;
import org.hibernate.cfg.Configuration;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Example;
import org.hibernate.service.ServiceRegistry;
import org.hibernate.service.ServiceRegistryBuilder;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by Dimitri on 10/05/2014.
 */
@SuppressWarnings("unchecked")
public abstract class AbstractDao<T, ID extends Serializable> implements Serializable {

    protected Class<T> persistentClass;
    //protected Session session;

    private static SessionFactory sessionFactory;
    private static ServiceRegistry serviceRegistry;

    static {

        try {

            Configuration configuration = new Configuration();
            configuration.configure();

            serviceRegistry = new ServiceRegistryBuilder().applySettings(configuration.getProperties()).buildServiceRegistry();
            sessionFactory = configuration.buildSessionFactory(serviceRegistry);
        } catch (Exception ex) {
            throw new ExceptionInInitializerError(ex);
        }

    }

    {

        this.persistentClass = (Class<T>) ((ParameterizedType) getClass()
                .getGenericSuperclass()).getActualTypeArguments()[0];
    }





    protected Session getSession() {
        return sessionFactory.openSession();
    }

    protected StatelessSession getStatelessSession() {
        return sessionFactory.openStatelessSession();
    }

    public Class<T> getPersistentClass() {
        return persistentClass;
    }

    public T findById(ID id) {
        return (T) getSession().load(getPersistentClass(), id);
    }

    public List<T> findAll() {
        return findByCriteria();
    }

    public List<T> findByExample(T exampleInstance, String[] excludeProperty) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        Example example =  Example.create(exampleInstance);
        for (String exclude : excludeProperty) {
            example.excludeProperty(exclude);
        }
        crit.add(example);
        return crit.list();
    }

    public T save(T entity) {
        final Session session = getSession();

        Transaction tx = null;

        try {
            session.beginTransaction();

            session.saveOrUpdate(entity);

            session.getTransaction().commit();


            session.evict(entity.getClass());

        }
        catch (NonUniqueObjectException nonUniqueException) {
            try {
                session.merge(entity);
                session.getTransaction().commit();
            }
            catch (HibernateException e) {
                if (session.getTransaction()!=null) {
                    session.getTransaction().rollback();
                }
                return null;
            }
        }
        catch (HibernateException e) {
            if (session.getTransaction()!=null) {
                session.getTransaction().rollback();
            }
            return null;
        }
        finally {
            session.close();
        }

        return entity;
    }





    public void makeTransient(T entity) {
        getSession().delete(entity);
    }

    public void flush() {
        getSession().flush();
    }

    public void clear() {
        getSession().clear();
    }

    /**
     * Use this inside subclasses as a convenience method.
     */
    protected List<T> findByCriteria(Criterion... criterion) {
        Criteria crit = getSession().createCriteria(getPersistentClass());
        for (Criterion c : criterion) {
            crit.add(c);
        }
        return crit.list();
    }

}
