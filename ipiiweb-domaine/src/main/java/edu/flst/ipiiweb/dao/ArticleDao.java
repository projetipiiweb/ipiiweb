package edu.flst.ipiiweb.dao;

import edu.flst.ipiiweb.persistence.Article;
import edu.flst.ipiiweb.persistence.User;
import org.hibernate.CacheMode;
import org.hibernate.FlushMode;
import org.hibernate.Hibernate;
import org.hibernate.Session;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dimitri on 11/05/2014.
 */
@SuppressWarnings("unchecked")
public class ArticleDao extends AbstractDao<Article, Integer> {

    /*public ArticleDao() {
        super();
    }*/


    public List<Article> findPremiumArticles() {
        Session session = getSession();

        return (ArrayList<Article>) session.createQuery("select a from Article a where a.desactive = true")
                .list();
    }

    public List<Article> findAllValidArticles() {


        Session session = getSession();

        List<Article> articles = null;

        try {

            articles = session.createQuery("select a from Article a where a.desactive = :isDesactive")
                    .setParameter("isDesactive", false)
                    .list();

            session.setCacheMode(CacheMode.IGNORE);

            for (Article article : articles) {
                Hibernate.initialize(article.getCodesPromo());
            }

        }
        catch (Exception e ) {
            //return null
        }
        finally {
            session.close();
        }

        return articles;
    }

    public User deleteArticle(int id) {

        Session session = getSession();

        return (User) session.createQuery("update Article a set a.desactive = :isDesactive where a.id = :articleId")
                .setParameter("isDesactive", true)
                .setParameter("articleId", id)
                .uniqueResult();
    }
}
